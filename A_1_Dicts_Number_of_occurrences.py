print("        UNIVERSIDAD NACIONAL DE LOJA  ")
print("Autora: Lilia Susana Tene")
print("Email: liliatene@unl.edu.ec")
print("    -+-+-+-+-+- DICTS -+-+-+-+-+-")

#Statement
#The text is given in a single line. For each word of the text count the number of its occurrences before it.

# Print a value:
# print(s)
s=input()
def occu(s):
    contador = {}
    string = ''
    for palabra in s.split():
        contador[palabra] = contador.get(palabra, 0) + 1
        string += str(contador[palabra] - 1) +' '
    return string
print(occu(s))