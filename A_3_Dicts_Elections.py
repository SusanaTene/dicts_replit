#Statement
#The first line contains the number of records. After that, each entry contains the name of the candidate
# and the number of votes they got in some state. Count the results of the elections: sum the number of votes
# for each candidate. Print candidates in the alphabetical order.

a_voto = {}
for _ in range(int(input())):
    candidatos, voto = input().split()
    a_voto[candidatos] = a_voto.get(candidatos, 0) + int(voto)

for candidatos, voto in sorted(a_voto.items()):
    print(candidatos, voto)
