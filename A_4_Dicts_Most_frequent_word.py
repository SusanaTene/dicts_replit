#Statement
#Given the text: the first line contains the number of lines, then given the lines of words. Print the
#word in the text that occurs most often. If there are many such words, print the one that is less in the
# alphabetical order.

# Read a string:
s = int(input())
# Print a value:
# print(s)
nombres ={}
myList = []
for i in range(s):
    a = list(input().split())
    for t in range(len(a)):
        if a[t] not in nombres:
            nombres[a[t]] = 0
        nombres[a[t]] += 1
for k,v in nombres.items():
    if v >= max(nombres.values()):
        myList.append(k)
print(sorted(myList)[0])