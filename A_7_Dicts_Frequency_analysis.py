#Statement
#Given a number n, followed by n lines of text, print all words encountered in the text, one per line, with their
# number of occurrences in the text. The words should be sorted in descending order according to their number of
# occurrences, and all words within the same frequency should be printed in lexicographical order.

#Hint. After you created a dictionary of the words and their frequencies, you would like to sort it according to
# the frequencies. This can be achieved if you create a list whose elements are lists of two elements: the number
# of occurrences of a word and the word itself. For example, [[2, 'hi'], [1, 'what'], [3, 'is']]. Then the standard
# list sort will sort a list of lists, with the lists compared by the first element, and if these are equal, by
# the second element. This is nearly what is required.

# Read a string:
a = int(input())
# Print a value:
# print(s)
num = {}
for _ in range(a):
  for ingreso in input().split():
    num[ingreso] = num.get(ingreso, 0) + 1
frecuencia = [(-num, ingreso) for (ingreso, num) in num.items()]
for c, ingreso in sorted(frecuencia):
    print(ingreso,c)
